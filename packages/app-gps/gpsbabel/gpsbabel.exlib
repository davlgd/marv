# Copyright 2015 Mykola Orliuk <virkony@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ tag="${PN}_$(ever replace_all _)" ]
require cmake

SUMMARY="GPS waypoint, route and track data converter"
DESCRIPTION="
GPSBabel reads, writes, and manipulates GPS waypoints in a variety of formats. Over 100 supported
formats include GPX, Magellan and Garmin serial and USB protocols, Geocaching *.loc, Garmin
Mapsource, and Magellan Mapsend. For Windows, Linux, Mac.
"
HOMEPAGE="https://www.gpsbabel.org"
UPSTREAM_CHANGELOG="${HOMEPAGE}/changes.html"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libusb:1
        sys-libs/zlib
        x11-libs/qtbase:5[>=5.12]
        x11-libs/qtserialport:5
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DGPSBABEL_WITH_SHAPELIB:STRING=included
    -DGPSBABEL_WITH_LIBUSB:STRING=pkgconfig
    -DGPSBABEL_WITH_ZLIB:STRING=findpackage

    # Needs qtwebengine
    -DGPSBABEL_MAPPREVIEW:BOOL=OFF
)

src_install() {
    # There's no install target
    dobin gpsbabel

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

