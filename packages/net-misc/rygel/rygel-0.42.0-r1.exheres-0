# Copyright 2011 Dan Callaghan <djc@djc.id.au>
# Copyright 2022 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Partly derived from rygel-0.9.1.ebuild, which is:
#   Copyright 1999-2011 Gentoo Foundation
#   Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gtk-icon-cache freedesktop-desktop
require meson

SUMMARY="UPnP AV (aka DLNA) implementation"
HOMEPAGE="https://wiki.gnome.org/Projects/Rygel"

LICENCES="
    LGPL-2.1
    CCPL-Attribution-ShareAlike-3.0 [[ note = [ Rygel Logo ] ]]
"
SLOT="2.8"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    gui
    tracker
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.33.4] )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        base/libgee:0.8[>=0.8.0]
        dev-db/sqlite:3[>=3.5]
        dev-libs/glib:2[>=2.62.0][gobject-introspection(+)?]
        dev-libs/libunistring:=
        dev-libs/libxml2:2.0[>=2.7.0]
        gnome-desktop/libsoup:3.0[>=2.44.0][gobject-introspection][vapi]
        media-libs/gstreamer:1.0[>=1.0.0][gobject-introspection]
        media-libs/libmediaart:2.0[>=0.7.0][gobject-introspection][vapi]
        media-plugins/gst-plugins-base:1.0[>=1.0.0][gobject-introspection]
        media-plugins/gstreamer-editing-services:1.0[>=1.16.0][gobject-introspection]
        net-libs/gssdp:1.6[>=1.5.0][gobject-introspection][vapi]
        net-libs/gupnp-av:1.0[>=0.14.1][gobject-introspection][vapi]
        net-libs/gupnp-dlna:2.0[>=0.9.4][gobject-introspection][vapi]
        net-libs/gupnp:1.6[>=1.5.2][gobject-introspection][vapi]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/libX11
        gui? ( x11-libs/gtk+:3[>=3.0.0] )
        tracker? ( app-pim/tracker:3.0 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dapi-docs=false
    -Dman_pages=true
    -Dengines=gstreamer
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    'gui gtk'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_configure() {
    local plugins=()

    plugins+=(
        external
        gst-launch
        lms
        media-export
        mpris
        playbin
    )

    if option tracker; then
        plugins+=( tracker3 )
    fi

    meson_src_configure \
        -Dplugins=$(IFS=,; echo "${plugins[*]}")
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

