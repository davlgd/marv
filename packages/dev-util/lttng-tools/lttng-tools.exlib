# Copyright 2021 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lttng tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A set of tools to control LTTng tracing"
DESCRIPTION="
The project includes the LTTng session daemon, consumer daemon and relay daemon, as well as
'liblttng-ctl', a C library used to communicate with the session daemon, and 'lttng', a command line
interface to 'liblttng-ctl'.
"
HOMEPAGE="https://lttng.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0[>=2.7.6]
        dev-libs/popt
        dev-libs/urcu[>=0.11]
        dev-util/lttng-ust[>=2.12.0]
        sys-apps/kmod
"

RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-Werror
    --disable-python-bindings
    --disable-embedded-help

    --enable-man-pages

    --with-kmod
    --with-lttng-ust
)

src_test() {
    esandbox allow_net "unix:/tmp/test.unix.socket.creds.passing.*/test_unix_socket"

    default

    esandbox disallow_net "unix:/tmp/test.unix.socket.creds.passing.*/test_unix_socket"
}

