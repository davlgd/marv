# Copyright 2019, 2020 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PV} ]
require cmake
require alternatives

myexparam llvm_slot=
exparam -v LLVM_SLOT llvm_slot

export_exlib_phases src_prepare src_install

SUMMARY="Find unnecessary include directives in C/C++ programs"
HOMEPAGE+=" https://include-what-you-use.org/"

LICENCES="GPL-2"
SLOT="${LLVM_SLOT}"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/llvm:${LLVM_SLOT}
        dev-lang/clang:${LLVM_SLOT}
"

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${LLVM_SLOT}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # This is crucial for the tests to work since they copy clang's builtin headers from
    # ${CMAKE_PREFIX_PATH}/lib/clang/${llvm_ver}/include to the build directory
    -DCMAKE_PREFIX_PATH:PATH="${LLVM_PREFIX}"

    -DCMAKE_INSTALL_PREFIX:PATH="${LLVM_PREFIX}"
)

include-what-you-use_src_prepare() {
    default

    # Only use ${SLOT} as LLVM/clang version since clang installs its builtin headers to
    # `lib/clang/${SLOT}` and not `lib/clang/${CLANG_VERSION}` on Exherbo
    edo sed -e "s:\(set(llvm_ver\) .*:\1 ${LLVM_SLOT}):" \
            -i "${CMAKE_SOURCE}"/CMakeLists.txt
}

include-what-you-use_src_install() {
    cmake_src_install

    # Symlink binaries to /usr/$(exhost --target)/bin (without .py extension and with -${SLOT}
    # suffix, e.g. `iwyu-tool-${SLOT}`)
    dodir "/usr/$(exhost --target)/bin"
    edo pushd "${IMAGE}${LLVM_PREFIX}/bin"
    for bin in *; do
        dosym "${LLVM_PREFIX}/bin/${bin}" "/usr/$(exhost --target)/bin/${bin%*.py}-${SLOT}"
    done
    edo popd

    # alternatives for different slots
    local alternatives=()
    local f

    for f in iwyu_tool include-what-you-use fix_includes; do
        alternatives+=( /usr/$(exhost --target)/bin/${f} ${f}-${SLOT} )
    done

    alternatives+=( /usr/share/man/man1/${PN}.1 ${PN}-${SLOT}.1 )
    alternatives+=( /usr/share/${PN} ${PN}-${SLOT} )

    alternatives_for ${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

